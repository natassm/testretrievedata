package com.example.plannerproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListAdapter.ToDoListViewHolder> {

    Context context;
    ArrayList<ToDoList> toDoListArrayList;


    public ToDoListAdapter(Context c, ArrayList<ToDoList> p){
        context = c;
        toDoListArrayList = p;
    }

    @NonNull
    @Override
    public ToDoListViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ToDoListViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_holder_item_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ToDoListViewHolder toDoListViewHolder, int position) {
        ToDoList toDoList = toDoListArrayList.get(position);
        toDoListViewHolder.titledoes.setText(toDoList.getTitledoes());
        toDoListViewHolder.descdoes.setText(toDoList.getDescdoes());
    }

    @Override
    public int getItemCount() {
        return toDoListArrayList.size();
    }

    public class ToDoListViewHolder extends RecyclerView.ViewHolder{

        private TextView titledoes, descdoes;

         public ToDoListViewHolder(View itemView) {
            super(itemView);

             titledoes = itemView.findViewById(R.id.itemListTitleTextView);
             descdoes = itemView.findViewById(R.id.itemListDescTextView);
        }
    }

}

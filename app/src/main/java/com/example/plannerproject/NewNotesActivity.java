package com.example.plannerproject;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class NewNotesActivity extends AppCompatActivity {

    EditText titleET, descET;
    Button submitBtn;
    String sTitle, sDesc;
    DatabaseReference reference;

    ToDoList tdl  = new ToDoList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notes);

        titleET = findViewById(R.id.newTitleDoesEditText);
        descET = findViewById(R.id.newDescDoesEditText);
        submitBtn = findViewById(R.id.newSaveButton);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sTitle = titleET.getText().toString();
                sDesc = descET.getText().toString();

                reference = FirebaseDatabase.getInstance().getReference().child("ToDoLists");

                getValue();

                if (titleET.equals("") || descET.equals("")){
                    Toast.makeText(NewNotesActivity.this, "Please fill all the data",
                            Toast.LENGTH_LONG).show();
                }else{
                    reference.child("ToDoListMenu").push().setValue(tdl);
                    titleET.setText("");
                    descET.setText("");
                    Toast.makeText(NewNotesActivity.this, "Data saved", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getValue(){
        tdl.setTitledoes(titleET.getText().toString());
        tdl.setDescdoes(descET.getText().toString());
    }
}

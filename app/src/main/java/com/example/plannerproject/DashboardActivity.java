package com.example.plannerproject;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;

public class DashboardActivity extends AppCompatActivity {

    RecyclerView listRV;
    FloatingActionButton btnAddNew;

    DatabaseReference reference;
    ToDoListAdapter toDoListAdapter;
    ArrayList<ToDoList> toDoListArrayList = new ArrayList<ToDoList>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        btnAddNew = findViewById(R.id.dashboardAddButton);
        toDoListAdapter = new ToDoListAdapter(this, toDoListArrayList);
        listRV = findViewById(R.id.dashboardListRecyclerView);
        listRV.setLayoutManager(new LinearLayoutManager(this));
        listRV.setItemAnimator(new DefaultItemAnimator());
        listRV.setAdapter(toDoListAdapter);

        reference = FirebaseDatabase.getInstance().getReference().child("ToDoLists");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot dataSnapshot1: dataSnapshot.getChildren()){
                    for (DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                        ToDoList p = dataSnapshot2.getValue(ToDoList.class);
                        toDoListArrayList.add(p);
                    }
                }
                toDoListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getApplicationContext(), "No Data", Toast.LENGTH_SHORT).show();
            }
        });

        btnAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent a = new Intent(DashboardActivity.this, NewNotesActivity.class);
                startActivity(a);
            }
        });
    }
}

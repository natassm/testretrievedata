package com.example.plannerproject;

import androidx.annotation.NonNull;

public class ToDoList {

    String titledoes, descdoes;

    public ToDoList(){

    }

    public ToDoList(String titledoes, String descdoes) {
        this.titledoes = titledoes;
        this.descdoes = descdoes;
    }

    public String getTitledoes() {
        return titledoes;
    }

    public void setTitledoes(String titledoes) {
        this.titledoes = titledoes;
    }

    public String getDescdoes() {
        return descdoes;
    }

    public void setDescdoes(String descdoes) {
        this.descdoes = descdoes;
    }

    @NonNull
    @Override
    public String toString() {
        return "" + titledoes + "\n" + descdoes + "\n";
    }
}
